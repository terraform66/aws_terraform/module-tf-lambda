# ------------------------------------------------------------------------------
# Resources
# ------------------------------------------------------------------------------
resource "aws_lambda_function" "main" {
  function_name                   = var.name_prefix
  description                     = "terraform lambda function"
  filename                        = var.filename
  s3_bucket                       = var.s3_bucket
  s3_key                          = var.s3_key
  s3_object_version               = var.s3_object_version
  handler                         = var.handler
  source_code_hash                = var.source_code_hash
  runtime                         = var.runtime
  timeout                         = var.timeout
  memory_size                     = var.memory_size
  publish                         = var.publish
  role                            = aws_iam_role.main.arn
  rreserved_concurrent_executions = var.reserved_concurrent_executions
  layers                          = var.layers
  tags                            = merge(var.tags, { "Name" = var.name_prefix })

  vpc_config {
    subnet_ids         = var.subnet_ids
    security_group_ids = aws_security_group.vpc[*].id
  }

  dynamic "environment" {
    for_each = length(var.environment) > 0 ? [1] : []
    content {
      variables = var.environment
    }
  }
  layers = var.layers

  tags = merge(var.tags, { "Name" = var.name_prefix })

}

resource "aws_security_group" "vpc" {
  count       = length(var.subnet_ids) > 0 ? 1 : 0
  name        = "${var.name_prefix}-sg"
  description = "Terraformed security group."
  vpc_id      = var.vpc_id
  tags        = merge(var.tags, { "Name" = "${var.name_prefix}-sg" })
}


