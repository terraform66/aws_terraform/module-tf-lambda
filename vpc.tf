resource "aws_security_group" "vpc" {
  count       = length(var.subnet_ids) > 0 ? 1 : 0
  name        = "${var.name_prefix}-sg"
  description = "Terraformed security group."
  vpc_id      = var.vpc_id
  tags        = merge(var.tags, { "Name" = "${var.name_prefix}-sg" })
}