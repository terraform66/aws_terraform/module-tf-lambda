# module-tf-lambda

The terraform infrastructure for lambda. This module creates a lambda function and takes care of setting up the execution role, in addition to uploading the source code. Note that source_code_hash or s3_object_version must be set if you want to automatically update the Lambda execution code.

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.14 |

## Providers

| Name | Version |
|------|---------|
| aws | n/a |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| environment | A map that defines environment variables for the Lambda function. | `map(string)` | `{}` | no |
| filename | The path to the function's deployment package within the local filesystem. | `string` | `null` | no |
| handler | The function entrypoint in your code. | `string` | `"main"` | no |
| layers | A list of Lambda Layer Version ARNs (maximum of 5) to attach to your Lambda Function. | `list(string)` | `[]` | no |
| memory\_size | Amount of memory in MB your Lambda Function can use at runtime. | `number` | `128` | no |
| name\_prefix | A prefix used for naming resources. | `string` | n/a | yes |
| policy | A policy document for the lambda execution role. | `string` | n/a | yes |
| publish | Whether to publish creation/change as new Lambda Function Version. Defaults to false. | `bool` | `false` | no |
| reserved\_concurrent\_executions | The amount of reserved concurrent executions for this lambda function. A value of 0 disables lambda from being triggered and -1 removes any concurrency limitations. Defaults to Unreserved Concurrency Limits -1 | `number` | `-1` | no |
| runtime | Lambda runtime. Defaults to Go 1.x. | `string` | `"go1.x"` | no |
| s3\_bucket | The bucket where the lambda function is uploaded. | `string` | `null` | no |
| s3\_key | The s3 key for the Lambda artifact. | `string` | `null` | no |
| s3\_object\_version | The object version containing the function's deployment package. Conflicts with filename. | `string` | `null` | no |
| source\_code\_hash | Used to trigger updates. Must be set to a base64-encoded SHA256 hash of the package file specified with either filename or s3\_key. | `string` | `null` | no |
| subnet\_ids | A list of subnet IDs associated with the Lambda function. | `list(string)` | `[]` | no |
| tags | A map of tags (key-value pairs) passed to resources. | `map(string)` | `{}` | no |
| timeout | The amount of time your Lambda Function has to run in seconds. | `number` | `300` | no |
| vpc\_id | The VPC ID. | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| arn | The Amazon Resource Name (ARN) identifying your Lambda Function. |
| invoke\_arn | The ARN to be used for invoking Lambda Function from API Gateway - to be used in aws\_api\_gateway\_integration uri. |
| name | The name of the lambda function. |
| qualified\_arn | The Amazon Resource Name (ARN) identifying your Lambda Function Version (if versioning is enabled via publish = true). |
| role\_arn | The Amazon Resource Name (ARN) specifying the execution role. |
| role\_name | The name of the execution role. |
| security\_group\_id | The ID of the security group. |

